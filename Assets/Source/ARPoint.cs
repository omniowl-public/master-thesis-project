﻿using GoogleARCore;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARPoint : MonoBehaviour
{
    internal Trackable m_Trackable;

    private MeshRenderer m_MeshRenderer;

    private void Awake()
    {
        m_MeshRenderer = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (m_Trackable.TrackingState != TrackingState.Tracking)
        {
            m_MeshRenderer.enabled = false;
            return;
        }

        m_MeshRenderer.enabled = true;
    }
}
