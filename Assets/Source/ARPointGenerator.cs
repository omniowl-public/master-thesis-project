﻿
using System.Collections.Generic;
using GoogleARCore;
using GoogleARCore.Examples.Common;
using UnityEngine;
using UnityEngine.UI;

public class ARPointGenerator : MonoBehaviour
{
    public Camera FirstPersonCamera;
    public GameObject DetectedPlanePrefab;
    public GameObject DetectedFeaturePointPrefab;
    public Text featuredPointsInstantiatedText;
    public Text pointsInstantiatedText;
    public Text pointCloudCountText;
    public Button clearButton;

    private HashSet<int> featurePointIds = new HashSet<int>();
    private HashSet<int> pointIds = new HashSet<int>();
    private HashSet<GameObject> m_StoredGameObjects = new HashSet<GameObject>();
    private bool IsClearRequested = false;

    public void ClearStoredPoints()
    {
        IsClearRequested = true;
        clearButton.interactable = false;
    }

    public void Update()
    {
        if (Session.Status != SessionStatus.Tracking)
        {
            return;
        }
        pointCloudCountText.text = $"{Frame.PointCloud.PointCount}";

        if (IsClearRequested == true)
        {
            IsClearRequested = false;
            foreach (GameObject go in m_StoredGameObjects)
            {
                Destroy(go);
            }
            m_StoredGameObjects.Clear();
            featurePointIds.Clear();
            pointIds.Clear();
            clearButton.interactable = true;
        }

        for (int i = 0; i < Frame.PointCloud.PointCount; i++)
        {
            PointCloudPoint point = Frame.PointCloud.GetPointAsStruct(i);
            if (featurePointIds.Contains(point.Id) || pointIds.Contains(point.Id))
            {
                continue;
            }
            Vector3 pos = FirstPersonCamera.WorldToScreenPoint(point);
            TrackableHit hit;
            TrackableHitFlags raycastFilter = TrackableHitFlags.Default;
            if (Frame.Raycast(pos.x, pos.y, raycastFilter, out hit))
            {
                if ((hit.Trackable is Trackable) &&
                Vector3.Dot(FirstPersonCamera.transform.position - hit.Pose.position,
                    hit.Pose.rotation * Vector3.up) < 0)
                {
                    Debug.Log("Hit at back of the current DetectedPlane");
                }
                else
                {
                    GameObject prefab;
                    Anchor anchor = hit.Trackable.CreateAnchor(hit.Pose);
                    if (hit.Trackable is FeaturePoint)
                    {
                        featurePointIds.Add(point.Id);
                        prefab = DetectedFeaturePointPrefab;
                    }
                    else
                    {
                        pointIds.Add(point.Id);
                        prefab = DetectedPlanePrefab;
                    }

                    GameObject pointVisualizer = Instantiate(prefab, hit.Pose.position, hit.Pose.rotation);
                    pointVisualizer.GetComponent<ARPoint>().m_Trackable = hit.Trackable;
                    pointVisualizer.transform.Rotate(0, 180.0f, 0, Space.Self);
                    pointVisualizer.transform.parent = anchor.transform;
                    m_StoredGameObjects.Add(pointVisualizer);
                }
            }
        }
        featuredPointsInstantiatedText.text = $"{featurePointIds.Count}";
        pointsInstantiatedText.text = $"{pointIds.Count}";
    }
}
